﻿using Chaotic.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Repositories {
    public class CreatureService: Service<Creature> {
        public CreatureService( IMongoDatabase database ) : base( database ) {
        }
    }
}

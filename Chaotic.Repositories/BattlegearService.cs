﻿using Chaotic.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Repositories {
    public class BattlegearService: Service<Battlegear> {
        public BattlegearService( IMongoDatabase database ) : base( database ) {
        }
    }
}

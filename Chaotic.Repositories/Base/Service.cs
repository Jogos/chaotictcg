﻿using Chaotic.Models.Base;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Chaotic.Repositories {
    public abstract class Service<T> where T : Entity {
        private readonly IMongoCollection<T> _collection;

        public Service( IMongoDatabase database ) {
            _collection = database.GetCollection<T>(typeof(T).Name);
        }

        public virtual async Task<IEnumerable<T>> GetAsync( Expression<Func<T, bool>> filter = null ) {
            if ( filter == null )
                filter = ( item ) => true;
            return await ( await _collection.FindAsync( filter ) ).ToListAsync( );
        }

        public virtual async Task<T> GetAsync( Guid id ) =>
            await (await _collection.FindAsync<T>( item => item.Id == id ))
            .FirstOrDefaultAsync( );

        public virtual async Task<T> AddAsync( T item ) {
            await _collection.InsertOneAsync( item );
            return item;
        }

        public virtual async Task<IEnumerable<T>> AddAsync( IEnumerable<T> itens ) {
            await _collection.InsertManyAsync( itens );
            return itens;
        }

        public virtual Task UpdateAsync( Guid id, T newItem ) =>
            _collection.ReplaceOneAsync( item => item.Id == id, newItem );

        public virtual Task RemoveAsync( T bookIn ) =>
            _collection.DeleteOneAsync( item => item.Id == bookIn.Id );

        public virtual Task RemoveAsync( Guid id ) =>
            _collection.DeleteOneAsync( item => item.Id == id );
    }
}

﻿using Chaotic.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Repositories {
    public class LocationService: Service<Location> {
        public LocationService( IMongoDatabase database ) : base( database ) {
        }
    }
}

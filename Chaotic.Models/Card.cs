﻿using Chaotic.Models.Base;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Models {
    public abstract class Card : Entity {
        public virtual string Name { get; set; }

        [BsonIgnore]
        public virtual string Path => "";
    }
}

﻿using Chaotic.Models.Base;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chaotic.Models {
    public class Battlegear: Card {
        

        [BsonRepresentation( BsonType.String )]
        public Set Set { get; set; }

        public string SetId { get; set; }

        [BsonRepresentation( BsonType.String )]
        public Rarity Rarity { get; set; }

        public IEnumerable<string> Abilities { get; set; }

        public string Flavor { get; set; }
        
        public bool Unique { get; set; }
        
        public bool Loyal { get; set; }
        
        public bool Legendary { get; set; }
        
        public bool Past { get; set; }
        
        public bool Shard { get; set; }

        public override string Path => $"/cards/{Enum.GetName( typeof( Set ), Set )}/{SetId}.png";
    }
}
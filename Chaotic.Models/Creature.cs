﻿using Chaotic.Models.Base;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chaotic.Models {
    [BsonIgnoreExtraElements]
    public class Creature: Card {


        [BsonRepresentation(BsonType.String)]
        public Set Set { get; set; }

        public string SetId { get; set; }

        [BsonRepresentation(BsonType.String)]
        public Rarity Rarity { get; set; }

        [BsonRepresentation(BsonType.String)]
        public Tribe Tribe{ get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public Gender Gender{ get; set; }

        [BsonRepresentation( BsonType.String )]
        public IEnumerable<Type> Types { get; set; }
        
        public string Flavor { get; set; }
        
        public int Energy { get; set; }

        public int MugicCounters { get; set; }

        public Discipline Disciplines { get; set; }

        public Element Elements { get; set; }
        public bool Unique { get; set; }
        public bool Loyal { get; set; }
        public bool Legendary { get; set; }

        public override string Path => $"/cards/{Enum.GetName( typeof( Set ), Set )}/{SetId}.png";

        public IEnumerable<string> Abilities { get; set; }
        public IEnumerable<string> Brainwashed { get; set; }
    }
}

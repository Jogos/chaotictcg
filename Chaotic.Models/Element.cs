﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Models {
    public class Element {
        public bool Fire { get; set; }
        public bool Water { get; set; }
        public bool Earth { get; set; }
        public bool Air { get; set; }
    }
}

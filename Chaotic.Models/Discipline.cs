﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Models {
    public class Discipline {
        public int Courage { get; set; }
        public int Power { get; set; }
        public int Wisdom { get; set; }
        public int Speed { get; set; }
    }
}

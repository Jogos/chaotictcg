﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Models {
    public enum CardType { Creature, Battlegear, Mugic, Attack, Location }
    public enum Set { DOP, ZOTH, SS, MI, ROTO, TOTT, FUN, AU, FAS, OP1, BR, LR }
    public enum Rarity { Common, Uncommon, Rare, SuperRare, UltraRare, Promo }
    public enum Tribe { Generic, OverWorld, UnderWorld, Mipedian, Danian, Marrillian }
    public enum Gender { Male, Female, Ambiguous}
    public enum Initiative { 
        Air, 
        Chieftain,
        Courage,
        Danian,
        Earth,
        Elementalist,
        FewestElements,
        Fire,
        Fluidmorpher,
        Invisiblity,
        Marrillian,
        Minion,
        Mipedian,
        Muge,
        MugicCounters,
        MugicAbility,
        NumberOfElements,
        Overworld,
        Past,
        Power,
        ScannedEnergy,
        Speed,
        UnderWorld,
        Warbeast,
        Water,
        Wisdom
    }

    public enum Type { 
        Ambassador, 
        Battlemaster, 
        Minion, 
        Muge, 
        Warrior,
        Caretaker,
        Chieftain, 
        Commander, 
        Scout, 
        Conjuror,
        Conqueror,
        Controller, 
        Elementalist,
        Elite, 
        Stalker, 
        Ethereal,
        Fluidmorpher, 
        Guardian,
        Hero, 
        Kharall,
        Mandiblor, 
        Millaiin, 
        Noble, 
        Strategist, 
        Past,
        Beast,
        Royal,
        Squadleader,
        Taskmaster,
        Warbeast
    }
}

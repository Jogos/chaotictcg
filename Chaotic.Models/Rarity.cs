﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chaotic.Models {
    public class Rarity {
        public int RarityId { get; set; }
        public string Name { get; set; }

        public List<Creature> Creatures { get; set; }
        public List<Battlegear> Battlegears { get; set; }
        public List<Mugic> Mugics { get; set; }
        public List<Location> Locations { get; set; }
        public List<Attack> Attacks { get; set; }
    }
}

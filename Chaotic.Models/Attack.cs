﻿using Chaotic.Models.Base;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chaotic.Models {
    public class Attack : Card {

        [BsonRepresentation(BsonType.String)]
        public Set Set { get; set; }

        public string SetId { get; set; }

        [BsonRepresentation( BsonType.String )]
        public Rarity Rarity { get; set; }

        public int BuildPoints { get; set; }

        public int Base { get; set; }

        public int? Fire { get; set; }

        public int? Air { get; set; }

        public int? Earth { get; set; }

        public int? Water { get; set; }

        public IEnumerable<string> Abilities { get; set; }

        public string Flavor { get; set; }

        public bool Unique { get; set; }

        public override string Path => $"/cards/{Enum.GetName( typeof( Set ), Set )}/{SetId}.png";
    }
}

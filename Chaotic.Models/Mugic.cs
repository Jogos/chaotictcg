﻿using Chaotic.Models.Base;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chaotic.Models {
    public class Mugic: Card {


        [BsonRepresentation( BsonType.String )]
        public Set Set { get; set; }

        public string SetId { get; set; }

        [BsonRepresentation( BsonType.String )]
        public Rarity Rarity { get; set; }

        [BsonRepresentation( BsonType.String )]
        public Tribe Tribe { get; set; }

        public string Cost { get; set; }
        
        public IEnumerable<string> Abilities { get; set; }
        
        public string Flavor { get; set; }
        
        public bool Unique { get; set; }
        
        public bool Past { get; set; }

        public override string Path => $"/cards/{Enum.GetName( typeof( Set ), Set )}/{SetId}.png";
    }
}

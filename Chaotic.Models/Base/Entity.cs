﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Models.Base {
   public abstract class Entity {
        [BsonId]
        public virtual Guid Id { get; set; }

        public virtual DateTime CreatedAt { get; set; } = DateTime.Now;
        public virtual DateTime? UpdatedAt { get; set; } = null;
    }
}

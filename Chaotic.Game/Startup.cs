using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Chaotic.Data;
using MongoDB.Driver;
using Chaotic.Repositories;
using Plk.Blazor.DragDrop;
using Kraken.Component.Loader.Extensions;
using Chaotic.Models;

namespace Chaotic.Game {
    public class Startup {
        public Startup( IConfiguration configuration ) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices( IServiceCollection services ) {
            services.AddRazorPages( );
            services.AddServerSideBlazor( );
            services.AddBlazorDragDrop( );
            services.AddLoader( );

            services.AddSingleton<WeatherForecastService>( );

            services.AddSingleton<IMongoClient>( x => new MongoClient( Configuration.GetConnectionString( "DefaultConnection" ) ) );

            services.AddSingleton<IMongoDatabase>( ( serviceProvider ) => {
                var client = serviceProvider.GetService<IMongoClient>( );
                return client.GetDatabase( Configuration.GetValue<string>( "Database" ) );
            } );

            services.AddSingleton<CreatureService>( );
            services.AddSingleton<BattlegearService>( );
            services.AddSingleton<MugicService>( );
            services.AddSingleton<LocationService>( );
            services.AddSingleton<AttackService>( );

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IWebHostEnvironment env ) {
            if ( env.IsDevelopment( ) ) {
                app.UseDeveloperExceptionPage( );
            } else {
                app.UseExceptionHandler( "/Error" );
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts( );
            }

            app.UseHttpsRedirection( );
            app.UseStaticFiles( );

            app.UseRouting( );

            app.UseEndpoints( endpoints => {
                endpoints.MapBlazorHub( );
                endpoints.MapFallbackToPage( "/_Host" );
            } );
        }
    }
}

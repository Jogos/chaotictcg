# Chaotic TCG

**Progresso:** EM ANDAMENTO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2020<br />

### Objetivo
Implementar um servidor completo para controlar o jogo de cartas Chaotic.

### Observação

IDE:  [Visual Studio 2017](https://visualstudio.microsoft.com/)<br />
Linguagem: [C#](https://dotnet.microsoft.com/)<br />
Banco de dados: [MongoDB](https://www.mysql.com/)<br />

### Execução

    $ dotnet restore
    $ dotnet run
    

### Contribuição

Esse projeto está em andamento e aceita contribuições para o andamento, principalmente desenvolvedores Unity ou UnReal

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->
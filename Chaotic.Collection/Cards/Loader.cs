﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Chaotic.Collection.Cards {
    public class Loader {
        public static IEnumerable<TCardType> Load<TCardType, TCardMap>(string path ) where TCardMap : ClassMap {
            var conf = new CsvConfiguration( CultureInfo.InvariantCulture ) {
                Delimiter = ",",
                Quote = '\"'
            };
            conf.RegisterClassMap<TCardMap>( );

            using var reader = new StreamReader( path );
            using var csv = new CsvReader( reader, conf );
            csv.Read( );
            csv.ReadHeader( );
            var records = csv.GetRecords<TCardType>( ).ToList( );
            return records;
        }
    }
}

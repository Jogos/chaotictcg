﻿using Chaotic.Collection.Converter;
using Chaotic.Models;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Chaotic.Collection.Cards.Maps {
    class AttackMap: ClassMap<Attack> {
        public AttackMap( ) {
            Map( m => m.Name ).Name( "name" );
            Map( m => m.SetId ).Name( "id" );
            Map( m => m.Flavor ).Name( "flavor" );
            Map( m => m.Set ).Name( "set" ).TypeConverter<EnumConverter<Set>>( );
            Map( m => m.Rarity ).Name( "rarity" ).TypeConverter<EnumConverter<Rarity>>( );
            Map( m => m.BuildPoints ).Name( "bp" );
            Map( m => m.Base ).Name( "base" ).TypeConverter<IntConverter>();
            Map( m => m.Fire ).Name( "fire" );
            Map( m => m.Air ).Name( "air" );
            Map( m => m.Earth ).Name( "earth" );
            Map( m => m.Water ).Name( "water" );
            Map( m => m.Abilities ).Name( "ability" ).TypeConverter<StringMultipleLineConverter>( );
            Map( m => m.Unique ).Name( "unique" ).TypeConverter<BoolConverter>( );
        }
    }
}

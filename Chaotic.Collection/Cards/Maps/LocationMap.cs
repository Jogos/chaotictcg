﻿using Chaotic.Collection.Converter;
using Chaotic.Models;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Chaotic.Collection.Cards.Maps {
    class LocationMap: ClassMap<Location> {
        public LocationMap( ) {
            Map( m => m.Name ).Name( "name" );
            Map( m => m.SetId ).Name( "id" );
            Map( m => m.Flavor ).Name( "flavor" );
            Map( m => m.Initiative ).Name( "initiative" ).TypeConverter<EnumConverter<Initiative>>( );
            Map( m => m.Set ).Name( "set" ).TypeConverter<EnumConverter<Set>>( );
            Map( m => m.Rarity ).Name( "rarity" ).TypeConverter<EnumConverter<Rarity>>( );
            Map( m => m.Abilities ).Name( "ability" ).TypeConverter<StringMultipleLineConverter>();
            Map( m => m.Unique ).Name( "unique" ).TypeConverter<BoolConverter>();
            Map( m => m.Mirage ).Name( "mirage" ).TypeConverter<BoolConverter>();
            Map( m => m.Past ).Name( "past" ).TypeConverter<BoolConverter>();
        }
    }
}

﻿using Chaotic.Collection.Converter;
using Chaotic.Models;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Chaotic.Collection.Cards.Maps {
    class CreatureMap: ClassMap<Creature> {
        public CreatureMap( ) {
            Map( m => m.Name ).Name( "name" );
            Map( m => m.SetId ).Name( "id" );
            Map( m => m.Disciplines.Courage ).Name( "courage" );
            Map( m => m.Disciplines.Power ).Name( "power" );
            Map( m => m.Disciplines.Wisdom ).Name( "wisdom" );
            Map( m => m.Disciplines.Speed ).Name( "speed" );
            Map( m => m.MugicCounters ).Name( "mugic" );
            Map( m => m.Energy ).Name( "energy" );
            Map( m => m.Flavor ).Name( "flavor" );
            Map( m => m.Set ).Name( "set" ).TypeConverter<EnumConverter<Set>>( );
            Map( m => m.Rarity ).Name( "rarity" ).TypeConverter<EnumConverter<Rarity>>( );
            Map( m => m.Tribe ).Name( "tribe" ).TypeConverter<EnumConverter<Tribe>>( );
            Map( m => m.Gender ).Name( "gender" ).TypeConverter<EnumConverter<Gender>>( );
            Map( m => m.Elements ).Name( "elements" ).TypeConverter<ElementsConverter>();
            Map( m => m.Abilities ).Name( "ability" ).TypeConverter<StringMultipleLineConverter>();
            Map( m => m.Brainwashed ).Name( "brainwashed" ).TypeConverter<StringMultipleLineConverter>();
            Map( m => m.Types ).Name( "types" ).TypeConverter<EnumListConverter<Models.Type>>();
            Map( m => m.Unique ).Name( "unique" ).TypeConverter<BoolConverter>();
            Map( m => m.Loyal ).Name( "loyal" ).TypeConverter<BoolConverter>();
            Map( m => m.Legendary ).Name( "legendary" ).TypeConverter<BoolConverter>();
        }
    }
}

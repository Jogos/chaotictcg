﻿using Chaotic.Collection.Cards;
using Chaotic.Collection.Cards.Maps;
using Chaotic.Models;
using Chaotic.Repositories;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace Chaotic.Collection {
    class Program {
        static void Main( string[ ] args ) {
            var creatures = Loader.Load<Creature, CreatureMap>( "Collection/creatures.csv" );
            var battlegear = Loader.Load<Battlegear, BattlegearMap>("Collection/battlegears.csv" );
            var location = Loader.Load<Location, LocationMap>("Collection/locations.csv" );
            var mugics = Loader.Load<Mugic, MugicMap>("Collection/mugics.csv" );
            var attacks = Loader.Load<Attack, AttackMap>("Collection/attacks.csv" );
            
            Console.WriteLine( "CSVs parsed!" );

            var client = new MongoClient( "mongodb://localhost:27017" );
            var database = client.GetDatabase( "chaoticdb" );

            var creatureService = new CreatureService( database ).AddAsync( creatures );
            var battlegearService = new BattlegearService( database ).AddAsync( battlegear );
            var locationService = new LocationService( database ).AddAsync( location );
            var mugicService = new MugicService( database ).AddAsync( mugics );
            var attackService = new AttackService( database ).AddAsync( attacks );
           
            Task.WaitAll( 
                creatureService,
                battlegearService,
                locationService,
                mugicService,
                attackService
            );

            Console.WriteLine( "Database populated!" );
        }
    }
}

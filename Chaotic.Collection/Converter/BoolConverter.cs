﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chaotic.Collection.Converter {
    class BoolConverter: BooleanConverter {

        public override object ConvertFromString( string text, IReaderRow row, MemberMapData memberMapData ) {
            return text == "1";
        }
    }
}
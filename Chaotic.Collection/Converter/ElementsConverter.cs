﻿using Chaotic.Models;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;

namespace Chaotic.Collection.Converter {
    class ElementsConverter: DefaultTypeConverter {
        public override object ConvertFromString( string text, IReaderRow row, MemberMapData memberMapData ) {
            var elements = new Element {
                Air = text.Contains( "Air" ),
                Earth = text.Contains( "Earth" ),
                Fire = text.Contains( "Fire" ),
                Water = text.Contains( "Water" )
            };

            return elements;
        }
    }
}
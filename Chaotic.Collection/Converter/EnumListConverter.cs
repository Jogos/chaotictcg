﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chaotic.Collection.Converter {
    class EnumListConverter<T>: DefaultTypeConverter where T : struct {
        public override object ConvertFromString( string text, IReaderRow row, MemberMapData memberMapData ) {
            var result = new List<T>( );
            var list = text.Split( ' ', StringSplitOptions.RemoveEmptyEntries ).ToList( );

            foreach ( var item in list ) {
                var value = item.Replace( "'", "" ).Trim( );
                if ( Enum.TryParse<T>( value, true, out var response ) )
                    result.Add( response );
                else
                    throw new InvalidCastException( String.Format( "Invalid value to EnumConverter. Type: {0} Value: {1}", typeof( T ), text ) );
            }

            return result;
        }
    }
}
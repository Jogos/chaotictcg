﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chaotic.Collection.Converter {
    class IntConverter: Int32Converter {

        public override object ConvertFromString( string text, IReaderRow row, MemberMapData memberMapData ) {
            return string.IsNullOrEmpty( text ) ? 0 : Convert.ToInt32( text );
        }
    }
}
﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chaotic.Collection.Converter {
    class EnumConverter<T>: DefaultTypeConverter where T : struct {
        public override object ConvertFromString( string text, IReaderRow row, MemberMapData memberMapData ) {
            var value = text.Replace( " ", "" ).Replace( "'", "" ).Trim();
            if ( Enum.TryParse<T>( value, true, out var result ) ) 
                return result;            

            throw new InvalidCastException( String.Format( "Invalid value to EnumConverter. Type: {0} Value: {1}", typeof( T ), text ) );
        }
    }
}
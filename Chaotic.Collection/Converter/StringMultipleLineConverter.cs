﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chaotic.Collection.Converter {
    class StringMultipleLineConverter: CollectionGenericConverter {

        public override object ConvertFromString( string text, IReaderRow row, MemberMapData memberMapData ) {
            var list = text
                .Split( '\n', StringSplitOptions.RemoveEmptyEntries )
                .ToList( );

            list.ForEach( x => x.Trim('\n', '\r', ' ' ) );

            return list;
        }
    }
}